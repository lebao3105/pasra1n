# Makefile for pasra1n:>

# Compiler and its flags
C=fpc
# -Xs = auto strip at program compile time,
# -XX = smartlinking (ignore unneccessary code)
BUILD_FLAGS += -Xs -XX

# Windres, used to compile .rc file to .res
# x86_64-w64-mingw32-windres on Arch
# Now switched to fpcres - should come with FPC by default.
RCCOM = fpcres

# Install prefix and destination dir
PREFIX = /usr/local
DESTDIR =

ifeq ($(NO_SMARTLINK), 1)
	BUILD_FLAGS := $(filter-out -XX, $(BUILD_FLAGS))
endif

ifeq ($(NO_STRIP), 1)
	BUILD_FLAGS := $(filter-out -Xs, $(BUILD_FLAGS))
endif

pasra1n:
	$(RCCOM) src/pasra1n.rc -o src/pasra1n.res -of res
	$(C) $(BUILD_FLAGS) src/pasra1n.lpr -osrc/pasra1n

clean:
	$(RM) src/*.ppu src/*.o src/*.rsj src/pasra1n src/pasra1n.exe src/*.a src/*.res
	$(RM) -rf lib backup

distclean: clean

install: pasra1n
	install -D src/pasra1n $(DESTDIR)$(PREFIX)/bin/pasra1n

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/pasra1n

all: pasra1n

.PHONY: all pasra1n install clean distclean uninstall