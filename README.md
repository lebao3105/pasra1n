## Pasra1n

A command-line tool for Linux, BSD and macOS used for finding infos about iOS jailbreaks, apps bypass, device infomation, and more with AppleDB and Canister API libraries!

## How to build?

Read [BUILDING](./BUILDING).

## Usage

With --help / -h flag to show all available options.

## How can I contribute?

Like any other git repos, create a fork of this project, work your way, then make a PR.

Or, you can share issues/ideas!

Targeted works can be found in [TODO](./TODO).

More contributing notes can be found easily in [Contributing](./Contributing).

## License

This project is licensed under the BSD 3-Clause License.

[License](./LICENSE)