Unit info;
{
	(C) 2023-2024 Le Bao Nguyen and contributors.
	Licensed under the BSD 3-Clause License.
	File: info.pas
	Purpose: Contains "global" variables for the program.
}
{$mode objfpc}{$h+}

Interface

uses
	fpjson,
	jsonparser, sysutils,
	utilities;

var 
	//
	//	program flags
	//
	
	usesEmbeded: boolean = {$ifdef include_rc} true {$else} false {$endif};
	sayYes: boolean = false;
	interactive: boolean = false; // useful for GUI frontends

const
	version: string = '1.0.0';

implementation
var
	f: textfile;
	jsonSettings: TJSONObject;
	jsonData: TJSONData;
	s: string = '';
	tmps: string;

initialization
printdebug('Loading info.pas initialize tasks');

loadResourceItem('config', RT_RCDATA, 'settings.json');

AssignFile(f, ExtractFilePath(ParamStr(0)) + 'settings.json');
Reset(f);

while not EOF(f) do begin
	readln(f, tmps);
	s := s + tmps;
end;
Close(f);

jsonData := GetJSON(s);
jsonSettings := jsonData as TJSONObject;

printdebug('Loaded settings:');
printdebug(jsonData.FormatJSON);

writeln('Check for updates every ', jsonSettings.FindPath('pasra1n.updateEvery').AsString);
writeln('Last updated: ', jsonSettings.FindPath('pasra1n.lastUpdated').AsString);

jsonSettings.Free;
jsonData.Free;

printdebug('Done [info.pas]');
end.
