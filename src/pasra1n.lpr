program pasra1n;
{
	(C) 2023 Le Bao Nguyen and contributors.
	Licensed under the BSD 3-Clause License.
	File: pasra1n.pas
	Purpose: The main program entrypoint
}

{$mode objfpc}{$h+}
{$macro on}

{$r pasra1n.res}

uses baseunix,
	 {$ifdef unix} cthreads, {$endif}
	 classes, custapp, fpjson, jsonparser,
	 process, sysutils, variants,
	 info, resources, utilities;

type TRunner = class(TCustomApplication)
protected
	procedure DoRun; override;
end;

var 
	MyApp: TRunner;

procedure TRunner.DoRun;
var
	errorMsg: string;

begin
	Terminate;
	errorMsg := CheckOptions('hj:b:o:d:g:lv', ['help', 'jailbreak:', 'bypass:', 'osver:', 'device:', 'get:', 'list', 'version']);
	if errorMsg <> '' then raiseArgErr(errorMsg);

	if HasOption('h', 'help') then begin writeln(helpMsg); halt; end;
	if HasOption('v', 'version') then begin writeln(version); halt; end;
	if HasOption('i', 'interactive') then TryStrToBool(GetOptionValue('i', 'interactive'), interactive);
end;

begin
	MyApp := TRunner.Create(nil);
	MyApp.SetTitle('pasra1n');
	MyApp.StopOnException := true;
	MyApp.Run;
	MyApp.Free;
end.
