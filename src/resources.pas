unit resources;
{
	(C) 2023-2024 Le Bao Nguyen and contributors.
	Licensed under the BSD 3-Clause License.
	File: resources.pas
	Purpose: Contains resource strings and
	stuff for downloading and installing palera1n.
}

{$mode objfpc}{$H+}

Interface

uses
	baseunix, classes, fphttpclient, fpjson, jsonparser,
	openssl, opensslsockets, sysutils, strutils,
	info, utilities;

const
	AppVersion = 'pasra1n version %s';
	Credits = ' made by @lebao3105.' + sLineBreak
			+ 'A convenient tool for iOS jailbreaks and stuff.' + sLineBreak
			+ 'Used AppleDB and Canister API libraries.' + sLineBreak
			+ 'This software is licensed under the BSD 3-Clause license.';
	
	// use in sysutils.BoolToStr and sysutils.StrToBool.
	// store in sysutils.TrueBoolStrs

	TrueS = 'yes';
	FalseS = 'no';

	// program help

	helpMsg = 'Usage:' + sLineBreak
			+	'--help -h						  Show this help and exit' + sLineBreak
			+	'--interactive -i				  Interactive mode' + sLineBreak
			+	'--version -v					  Show the program version';

procedure raiseArgErr(text: string);

function downloadItem(const URL: ansistring): string; overload;
function downloadItem(const URL: ansistring; outPath: ansistring): boolean; overload;

Implementation
// used for initialization tasks
var
	testResponse: string;
	jData: TJSONData;
	jObj : TJSONObject;

procedure raiseArgErr(text: String);
begin
	writeln(Credits);
	writeln(helpMsg);
	error(text, 1);
end;

// https://forum.lazarus.freepascal.org/index.php?topic=65401.msg498273
// one more thing to care. {$H+}.
function downloadItem(const URL: ansistring): string;
begin
	result := TFPHTTPClient.SimpleGet(URL); // IDK why I made this, but whatever
end;

function downloadItem(const URL: ansistring; outPath: ansistring): boolean;
var
	fs: TFileStream;
	http: TFPHTTPClient;

begin
	http := TFPHTTPClient.Create(nil);
	http.AllowRedirect := true;
	FS := TFileStream.Create(outPath, fmCreate or fmOpenWrite);
	try
		http.Get(URL, FS);
	finally
		fs.Free;
		http.Free;
		result := true;
	end;
end;

initialization

printdebug('Loading resources.pas initialize tasks');

sysutils.TrueBoolStrs := [TrueS];
sysutils.FalseBoolStrs := [FalseS];

showinfo('Checking if Canister is fine...');

jData := GetJSON(downloadItem('https://api.canister.me/v2/'), true);
jObj := jData as TJSONObject;
testResponse := jObj.Get('status');
writeln(Format('Got status: %s', [testResponse]));

if (testResponse <> '200 OK') then
	error(Format('Unexpected response: %s', [testResponse]))
else
	showinfo('Canister is fine.');

showinfo('Also AppleDB... the API is big, please be paitent. An alternative way should be visible one day.');
testResponse := downloadItem('https://api.appledb.dev/main.json');

if (testResponse = '') then
	error('Got empty response!')
else
	showinfo('AppleDB is fine.');

jObj.Free;
printdebug('Done [resources.pas]');
end.
