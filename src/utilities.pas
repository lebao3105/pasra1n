unit utilities;
{
    (C) 2023-2024 Le Bao Nguyen and contributors.
    Licensed under the BSD 3-Clause license.
    File: utilities.pas
    Purpose: Procedures "utilities" functions
}

{$mode objfpc}{$h+}

interface

uses baseunix, classes,
     fpjson, jsonparser, sysutils, variants;

procedure showinfo(message: string); { Shows an infomation message }
procedure error(message: string); overload; { Shows an error message, but don't close the program }
procedure error(message: string; exitcode: integer); overload; { Shows an error message and exit }
procedure success(message: string); { Shows a "Completed!" message }
procedure warning(message: string); { Shows a warning }
procedure printdebug(message: string); { Shows a debug message, only if the DEBUG enviroment variable is set to 1. }
procedure input(message: string); { Indicates that the user needs to send their input here. With a message of course. }

function getOSType: string; { Get the current operating system type, e.g Linux or BSD. }
function WildcardSearch(const path: string): TStringArray; { Find for files (wildcard search) }

procedure clrscr; { Clear the console screen - uses escape characters. }

function loadResourceItem(name: string; kind: pchar; filename: string): boolean; {< Load an item linked to the program by a special file.}

implementation

procedure showinfo(message: string);
begin
    writeln(Format(#&33'[1;37m[Info] '#&33'[0m%s', [message]));
end;

procedure error(message: string);
begin
    writeln(Format(#&33'[1;31m[Error] '#&33'[0m%s', [message]));
end;

procedure error(message: string; exitcode: integer);
begin
    error(message);
    halt(exitcode);
end;

procedure success(message: string);
begin
    writeln(Format(#&33'[1;32m[Success] '#&33'[0m%s', [message]));
end;

procedure warning(message: string);
begin
    writeln(Format(#&33'[1;33m[Warning] '#&33'[0m%s', [message]));
end;

procedure printdebug(message: string);
begin
    {$ifndef DEBUG}
    if GetEnvironmentVariable('DEBUG') = '1' then
    {$endif}
        writeln(Format('[Debug] %s', [message]));
end;

procedure input(message: string);
begin
    write(Format(#&33'[1;37m[Input] '#&33'[0m%s ', [message]));
end;

function getOSType: string;
begin
    {$ifdef NOTWIN32}
        {$ifdef MACOSX}
        result := 'MacOS';
        {$else}
        result := 'UNIX-like (non macOS)';
        {$endif MACOSX}
    {$else}
    result := 'Windows';
    {$endif}
end;

function WildcardSearch(const path: string): TStringArray;
var
    Info: TSearchRec;
    Count: longint = 0;
    i: integer;
begin
    if FindFirst(path + '*', faAnyFile, Info) = 0 then
    begin
        repeat
            Inc(Count);
            SetLength(Result, Count);
            with Info do begin
                // writeln(Count, Name);
                Result[Count - 1] := Name;
            end;
        until FindNext(Info) <> 0;
        FindClose(info);
    end;
    for i := 0 to High(Result) do
        writeln(Result[i]);
end;

// Clear the screen
// From https://forum.lazarus.freepascal.org/index.php?topic=30878.0
procedure clrscr;
begin
    write(#27'[2J'#27'[1;1H');
end;

function loadResourceItem(name: string; kind: pchar; filename: string): boolean;
var
    s: TResourceStream;
    f: TFileStream;
begin
    result := false;
    s := TResourceStream.Create(HInstance, name, kind);
    // writeln(ExpandFileName(ExtractFilePath(ParamStr(0))) + filename);
    try
        f := TFileStream.Create(ExpandFileName(ExtractFilePath(ParamStr(0))) + filename, fmCreate);
        f.CopyFrom(s, s.size);
    finally
        f.Free;
        s.Free;
        result := true;
    end;
end;

initialization
printdebug('Loading utilities.pas');
printdebug('Done [utilities.pas]');
end.
